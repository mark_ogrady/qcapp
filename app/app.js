﻿(function () {
    'use strict';

    var qcapp = angular.module('QC', ['ngRoute', 'amplify']);


    qcapp.config(function ($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: 'app/views/user/login.html',
            controller: 'LoginController'
        }).when('/login', {

            templateUrl: 'app/views/user/login.html',
            controller: 'LoginController'
        }).when('/questionsets', {

            templateUrl: 'app/views/questionsets/questionsets.html',
            controller: 'QuestionSetsController'
        }).when('/questionset/:questionsetid', {

            templateUrl: 'app/views/questionsets/questionsetview.html',
            controller: 'QuestionSetViewController'
        }).when('/dashboard', {
                templateUrl: 'app/views/dashboard/dashboard.html',
                controller: 'DashboardController'
            });

    });

    qcapp.run(function($rootScope, AUTH_EVENTS, authService) {
        $rootScope.$on('$stateChangeStart', function(event, next) {
            var authorizedRoles = next.data.authorizedRoles;
            if (!AathService.isAuthorized(authorizedRoles)) {
                event.preventDefault();
                if (authService.isAuthenticated()) {
                    // user is not allowed
                    $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
                } else {
                    // user is not logged in
                    $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
                }
            }
        });
    });

    qcapp.config(function($httpProvider) {
        $httpProvider.interceptors.push([
            '$injector',
            function($injector) {
                return $injector.get('AuthInterceptor');
            }
        ]);
    });
    qcapp.factory('AuthInterceptor', function($rootScope, $q,
        AUTH_EVENTS) {
        return {
            responseError: function(response) {
                $rootScope.$broadcast({
                    401: AUTH_EVENTS.notAuthenticated,
                    403: AUTH_EVENTS.notAuthorized,
                    419: AUTH_EVENTS.sessionTimeout,
                    440: AUTH_EVENTS.sessionTimeout
                }[response.status], response);
                return $q.reject(response);
            }
        };
    });

    qcapp.constant('AUTH_EVENTS', {
        loginSuccess: 'auth-login-success',
        loginFailed: 'auth-login-failed',
        logoutSuccess: 'auth-logout-success',
        sessionTimeout: 'auth-session-timeout',
        notAuthenticated: 'auth-not-authenticated',
        notAuthorized: 'auth-not-authorized'
    });

})();