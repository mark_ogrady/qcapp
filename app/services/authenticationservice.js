﻿(function () {
    'use strict';
    var serviceId = 'session';

    angular.module('QC').service(serviceId, function () {
        this.create = function (user) {
            this.token = user.Token;
            this.email = user.Email;
            this.name = user.Name;
            this.userId = user.UserId;
            this.expirationDate = user.ExpirationDate;

        };
        this.destroy = function () {
            this.token = null;
            this.userId = null;

        };
        return this;
    });


})();

(function () {
    'use strict';

    var serviceId = 'authService';

    angular.module('QC').factory(serviceId, function ($http, session, localStorage) {
        var authService = {};

     

        authService.login = function(credentials) {
            return $http
                .post('http://localhost:50000/API/login', credentials)
                .then(function(res) {
                    console.log(res.data.ErrorMessage);
                if (!res.data.ErrorMessage) { //store the details away
                    localStorage.save('credentials', res.data);
                    session.create(res.data);
                }
                return res.data;
                
            });
        
        };

        authService.logout = function () {
            console.log("clearStore");
            localStorage.clearStore('credentials');
            
            return "logout";
        };


        authService.isAuthenticated = function() {
            return !!session.userId;
        };

        //authService.isAuthorized = function(authorizedRoles) {
        //    if (!angular.isArray(authorizedRoles)) {
        //        authorizedRoles = [authorizedRoles];
        //    }
        //    return (authService.isAuthenticated() &&
        //        authorizedRoles.indexOf(Session.userRole) !== -1);
        //};

        return authService;
    });


})();

