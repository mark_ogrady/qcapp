﻿(function () {
    'use strict';

    var controllerId = 'LoginController';


    angular.module('QC').controller(controllerId, ['$scope', 'authService', 'localStorage', '$route', '$rootScope', 'AUTH_EVENTS', logincontroller]);

    function logincontroller($scope, authService, localStorage, $route, $rootScope, AUTH_EVENTS) {

        console.log("Log");
        $scope.credentials = {
            username: '',
            password: ''
        };
        var userStore = localStorage.retrieve("credentials");
        if (userStore) {
            $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
            console.log(userStore);
            window.location = "#/dashboard";
        }


        $scope.activate = activate;
        $scope.login = function (credentials) {

            authService.login(credentials).then(function (user) {
                console.log("User" + user);
                if (user.ErrorMessage) {
                    console.log("loginFailedSite");
                    $scope.loginerror = user.ErrorMessage;
                    $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
                } else {
                    $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                    $scope.setCurrentUser(user);

                    window.location = "#/dashboard";
                }
            }, function () {
                console.log("loginFailed");
                $scope.loginerror = "Login Failed";
              
                $rootScope.$broadcast(AUTH_EVENTS.loginFailed);

            });
        };


        function activate() { }


    }
})();

