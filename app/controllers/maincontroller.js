﻿(function() {
    'use strict';

    var controllerId = 'maincontroller';


    angular.module('QC').controller(controllerId, ['$scope', 'authService', 'localStorage', maincontroller]);


    function maincontroller($scope, authService, localStorage) {
        $scope.currentUser = null;
        $scope.logout = null;
        $scope.isAuthorized = authService.isAuthorized;

        var userStore = localStorage.retrieve("credentials");
        console.log("Main - "+userStore);
        if (userStore)
        {$scope.currentUser = userStore;}

        $scope.setCurrentUser = function(user) {
            $scope.currentUser = user;
        }

        $scope.logout = function () {

            authService.logout();
            //    $scope.currentUser = null;
            //    console.log("redirect");
            //    window.location = "#/login";
            //}, function () {
               console.log("logoutFailed");
            //});
        };

        $scope.loggedin = function(event, message) {

        };


    };

   
})();