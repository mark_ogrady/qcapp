﻿(function () {
    'use strict';

    var controllerId = 'QuestionSetsController';

    angular.module('QC').controller(controllerId,['$scope', '$http','localStorage', questionSetsController]);


    function questionSetsController($scope, $http,localStorage) {
        $scope.title = 'Question Sets';
        $scope.activate = activate;
        $scope.storename = "QuestionSetsStore";
        $scope.questionSets = [];
        $http.get('http://localhost:50000/API/QuestionSetAPI/GetQuestionSets').success(function (data) {
            localStorage.save($scope.storename, data);
          
        });

        $scope.questionSets = localStorage.retrieve($scope.storename);
        function activate() { }


    }

})();

(function () {
    'use strict';

    var controllerId = 'QuestionSetViewController';

    angular.module('QC').controller(controllerId, ['$scope', '$http', 'localStorage','$routeParams', questionSetViewController]);


    function questionSetViewController($scope, $http, localStorage,$routeParams) {
        $scope.title = 'Question Set';
        $scope.activate = activate;
        $scope.storename = "QuestionSetsStore";
        $scope.QuestionSetId = $routeParams.questionsetid;
        $scope.questionSet = [];
        var questionSetList = localStorage.retrieve($scope.storename);

        questionSetList.filter(function(el, questionSetId ) {
            return el.QuestionSetId == questionSetId;
        });
        $scope.questionSet = questionSetList;

        
        function activate() { }


    }

})();